import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MainEventTest {

    @Test
    void testMain() {
        String[] args = new String[0];
        MainEvent test = new MainEvent();
        test.main(args);
    }
}