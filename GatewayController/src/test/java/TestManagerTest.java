import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class TestManagerTest {

    @Test
    public void invalidDiagnosticScript() {
        String wrongScript = "invalidTest.py";

        Throwable exception = assertThrows(AssertionError.class, () -> TestManager.getDiagnostic(wrongScript, "insertBattery"));
        //System.out.println(diagnostic);

    }

    @Test
    public void invalidEndpoint() {
        String endPoint = "insertInvalid";

        Throwable exception = assertThrows(AssertionError.class, () -> TestManager.getDiagnostic("memoryTest.py", endPoint));
        //System.out.println(diagnostic);

    }

    @Test
    public void validDiagnosticScriptInvalidEndpoint() {
        String diagnostic = "batteryTest.py";
        String endPoint = "insertInvalid";

        Throwable exception = assertThrows(AssertionError.class, () -> TestManager.getDiagnostic(diagnostic, endPoint));
        //System.out.println(diagnostic);

    }

    @Test
    public void invalidDiagnosticScriptValidEndpoint() {
        String diagnostic = "invalidTest.py";
        String endPoint = "insertBattery";

        Throwable exception = assertThrows(AssertionError.class, () -> TestManager.getDiagnostic(diagnostic, endPoint));
        //System.out.println(diagnostic);

    }
}