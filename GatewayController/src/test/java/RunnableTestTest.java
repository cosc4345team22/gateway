import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RunnableTestTest {

    @Test
    void runTest() {
//        String[] diagnosticsArray = new String[3];
//        diagnosticsArray[0] = "batteryTest.py";
//        diagnosticsArray[1] = "memoryTest.py";
//        diagnosticsArray[2] = "heartbeat.py";
//        int n = 3; // Number of threads
//        for (int i=0; i<n; i++)
//        {
////            RunnableTest diagnostic1 = new RunnableTest(diagnosticsArray[i]);
////            diagnostic1.start();
////            diagnostic1.run();
//////            String diagnosticResult = diagnostic1.run("memoryTest.py");
////            //System.out.println(diagnosticResult);
////            System.out.println(diagnostic1.getId());
//
//        }

    }
    @Test
    void initializeTest() {
        String diagnosticScript = "batteryTest.py";
        String endPoint = "insertBattery.py";
        RunnableTest test = new RunnableTest(diagnosticScript, endPoint);

        assertEquals(diagnosticScript, test.getDiagnosticScript());
        assertEquals(endPoint, test.getEndPoint());
    }
}