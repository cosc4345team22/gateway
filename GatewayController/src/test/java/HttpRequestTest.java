import com.google.gson.Gson;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HttpRequestTest {

    @org.junit.jupiter.api.Test
    void sendPostRequestTest() {
        HttpRequest postRequest = new HttpRequest();
        String response = "";
        String json = "{\"percent\": 100, \"secsleft\": -2, \"power_plugged\": true}";
        String endpoint = "insertBattery";
        try {
            response = postRequest.sendPost(json, endpoint);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert(response != "");
        System.out.println(response);

    }
}