import os

#install psutils if not installed on computer
# psutilInstall = "pip install psutil"
# os.system(psutilInstall)
import psutil
import sys
import json

def main():
    #memory check
    memoryFullCheck = psutil.disk_usage("/")
    memoryUsed = psutil.disk_usage("/").used
    memoryRemaining = psutil.disk_usage("/").free
    memoryUsedPercent = psutil.disk_usage("/").percent

    #print(memoryFullCheck)

    #create memoryDict from memoryFullCheck
    memoryDict = memoryFullCheck.__dict__

    #convert memoryDict to json
    jsonData = json.dumps(memoryDict)

    print(jsonData)

    #sys.stdout
    # sys.stdout.write("Memory Used:"+ str(memoryUsed)+"bytes  --  "+str(memoryUsedPercent)+ "%\n")
    # sys.stdout.write("Memory Remaining:"+str(memoryRemaining)+"bytes  --  "+str(100-memoryUsedPercent)+"%ln")
    #
    # #sys.sterror
    # sys.stderr = "IDK - none"

main()