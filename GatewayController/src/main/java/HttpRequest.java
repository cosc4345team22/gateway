import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class HttpRequest {

    private final String USER_AGENT = " ";


//    public static void main(String[] args) throws Exception {
//
//        HttpRequest httpRequest = new HttpRequest();
//
////        System.out.println("Testing 1 - Send Http GET request");
////        httpRequest.sendGet();
//
//        System.out.println("\nTesting 2 - Send Http POST request");
//        httpRequest.sendPost();
//
//    }

    // HTTP POST request
    public String sendPost(String json, String endPoint) throws Exception {

        // create url string with api endpoint and initialize URL obj
        String url = "https://team22.dev.softwareengineeringii.com/api/gateway/" + endPoint;
        URL obj = new URL(url);


        HttpsURLConnection connect = (HttpsURLConnection) obj.openConnection();

        //add request header
        connect.setRequestMethod("POST");
        connect.setRequestProperty("User-Agent", USER_AGENT);
        connect.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        connect.setRequestProperty("Content-Type", "application/json");

        // Send post request
        connect.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connect.getOutputStream());
        wr.write(json.getBytes("UTF-8")); //converts json string to bytes
        wr.close();

        // get response code and print response
        int responseCode = connect.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + json);
        System.out.println("Response Code : " + responseCode);


        // get response from buffer
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connect.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        //System.out.println(response.toString());

        //return response
        return response.toString();

    }

    // HTTP GET request
//    private void sendGet() throws Exception {
//
//        String url = "http://www.google.com/search?q=mkyong";
//
//        URL obj = new URL(url);
//        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//
//        // optional default is GET
//        con.setRequestMethod("GET");
//
//        //add request header
//        con.setRequestProperty("User-Agent", USER_AGENT);
//
//        int responseCode = con.getResponseCode();
//        System.out.println("\nSending 'GET' request to URL : " + url);
//        System.out.println("Response Code : " + responseCode);
//
//        BufferedReader in = new BufferedReader(
//                new InputStreamReader(con.getInputStream()));
//        String inputLine;
//        StringBuffer response = new StringBuffer();
//
//        while ((inputLine = in.readLine()) != null) {
//            response.append(inputLine);
//        }
//        in.close();
//
//        //print result
//        System.out.println(response.toString());
//
//    }

}
