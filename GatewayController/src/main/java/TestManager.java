import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestManager {

    public static void getDiagnostic(String diagnosticScript, String endPoint) {

        // for testing
        assert(diagnosticScript == "heartbeat.py" || diagnosticScript == "batteryTest.py"
                || diagnosticScript == "memoryTest.py" || diagnosticScript == "connectivityTest.py");

        assert(endPoint == "insertHeartbeat" || endPoint == "insertBattery" || endPoint == "insertCPU");

        if (diagnosticScript == "heartbeat.py") {
            assert (endPoint == "insertHeartbeat");
        }

        if (diagnosticScript == "memoryTest.py") {
            assert (endPoint == "insertCPU");
        }

        if (diagnosticScript == "batteryTest.py") {
            assert (endPoint == "insertBattery");
        }

        // create new RunnableTest object with requested diagnostic script and corresponding enpoint
        RunnableTest runDiagnostic = new RunnableTest(diagnosticScript, endPoint);
        Thread diagnosticThread = new Thread(runDiagnostic);
        diagnosticThread.start();

    }

    private static void bufferedReaderToStringBuilder(BufferedReader stdOut, StringBuilder stdoutString) {
        String s;
        try {
            while ((s = stdOut.readLine()) != null) {
                stdoutString.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }
    }
}
