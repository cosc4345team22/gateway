import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;


public class MainEvent {

    public static void main(String[] args){



        try {
            while (true) {

                // get heartbeat; getHeartbeat() returns heartbeat as json
                String heartbeat = getHeartbeat();


                // create HttpRequest and send POST request
                // with json heartbeat from heartbeat.py and endpoint
                // sendPost() will return HTTPS POST response as json string
                HttpRequest postRequest = new HttpRequest();
                String response = postRequest.sendPost(heartbeat, "insertHeartbeat");

                // create map out of post response
                Map<String, String> responseMap = jsonToMap(response);
                System.out.println("response map:" + responseMap.keySet()); // for testing

                // if post responseMap contains instruction for diagnostic, run diagnostic
                if (responseMap.containsKey("onDemand")) {
                    String endPoint ="";

                    // find corresponding enpoint for diagnostic
                    // by getting value associated with "diagnostic" key
                    // from responseMap
                    switch (responseMap.get("onDemand")) {
                        case "batteryTest.py":
                            endPoint = "insertBattery";
                            break;
                        case "memoryTest.py":
                            endPoint = "insertCPU";
                            break;
                        case "heartbeat.py":
                            endPoint = "insertHeartbeat";
                            break;
                        default:
                            endPoint = "";
                            break;

                    }
                    System.out.println("endpoint: " + endPoint); // print endpoint for testing

                    // call TestManager to getDiagnostic on requested diagnostic
                    // TestManager will create thread to post diagnostic
                    TestManager.getDiagnostic(responseMap.get("onDemand"), endPoint);

                }

                //set timer for loop control
                Thread.sleep(10 * 1000);


            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //
    private static void bufferedReaderToStringBuilder(BufferedReader stdOut, StringBuilder stdoutString) {
        String s;
        try {
            while ((s = stdOut.readLine()) != null) {
                stdoutString.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }
    }

    // creates a Map out of a json string
    private static Map<String, String> jsonToMap(String json) {
        Map<String, String> jsonMap = new Gson().fromJson(json, new HashMap<String, String>().getClass());
        return jsonMap;
    }

    // runs heartbeat.py and returns json string output
    private static String getHeartbeat() {

        String command = "";
        String prefix = "python";
        File testScriptFile = new File("../GatewayController/src/main/" + "heartbeat.py");
        try {
            command = prefix + " " + testScriptFile.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }
        Runtime rt = Runtime.getRuntime();
        Process proc = null;
        try {
            proc = rt.exec(command);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }

        if (proc == null) {
            System.err.println("TEST FAIL");
            throw new RuntimeException("Could not start process for test " + "heartbeat.py");
        }
        BufferedReader stdOut = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));

        // read the output from the command
        //System.out.println("STDOUT of " + diagnosticScript + ":\n");
        StringBuilder stdoutString = new StringBuilder();
        String s;
        bufferedReaderToStringBuilder(stdOut, stdoutString);
        //System.out.println(stdoutString + "\n");


        return String.valueOf(stdoutString);
    }

}