import os

import json

#install psutils if not installed on computer
# psutilInstall = "pip install psutil"
# os.system(psutilInstall)
import psutil
import sys

def secs2hours(secs):
    mm, ss = divmod(secs, 60)
    hh, mm = divmod(mm, 60)
    return "%d:%02d:%02d" % (hh, mm, ss)

def main():


    #initialize battery object with psutil
    battery = psutil.sensors_battery()


    #create battery dictionary
    batteryDict =  battery.__dict__

    #stdout
    # sys.stdout.write("charge = %s%%, time left = %s" % (battery.percent, secs2hours(battery.secsleft)))

    #convert batteryDict to json object and print
    jsonData=json.dumps(batteryDict)
    print (jsonData)

    #stderror
    #sys.stderr.write("No error")
main()