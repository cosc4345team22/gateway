import json

import time

import datetime

import subprocess

#class definition for heartbeat object
class Heartbeat:
    def __init__(self, gatewayId):
        self.timestamp=''
        self.gatewayId=gatewayId

    def addTimestamp(self, timestamp):
        self.timestamp=timestamp

#function to create a Heartbeat object from serialized dictionary data
def as_Heartbeat(heartbeatDict):
    newHeartbeat=Heartbeat(heartbeatDict['gatewayId'])
    newHeartbeat.addTimestamp(heartbeatDict['timestamp'])
    return newHeartbeat

def main():

    #create timestamp for heartbeat
    timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")


    #initialize Heartbeat object and add timestamp
    myHeartbeat=Heartbeat(123)
    myHeartbeat.addTimestamp(timestamp)


    #create Heartbeat dictionary (this makes it easy to convert object to json)
    heartbeatDict = myHeartbeat.__dict__

    #convert dictionary list to json
    jsondata=json.dumps(heartbeatDict)
    print (jsondata)

main()
